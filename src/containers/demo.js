import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Grid, ButtonBase, Link, CircularProgress } from "@material-ui/core";
import * as _ from "lodash";
import pdfIcon from "../assets/containers/icon/pdf.jpg";
import styles from "../assets/containers/style/demo";
import MarkdownEditor from '@uiw/react-markdown-editor';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';
import util from "../tool/util";
import config from "../config/config";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';
var signatureAction = require('../action/signatureAction');

const tempRules = `
{
  "signID": "proposer",
  "signerName": "Peter Chan",
  "lookupKey": "Signature",
  "height": "50px",
  "width": "135px",
  "offset": "60,0",
  "origin": "bottom-left",
  "repeat": false
}`;

const tempParam = `{
  "docID":"NB-3243545366",
  "localization": "CN"
}`;

class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionID: null,
      binary: "",
      docID: null,
      localization: "CN",
      signatureRules: null,
      iframe: true,
      iframeSrc: null,
      iframeOpen: false,
      error: null,
      complete: false,
      currentPage: 0,
      numPages: 0,
      requiredParameter: null,
      loading: false
    };
    this.handleUploadImage = this.handleUploadImage.bind(this);
    this.changeSignatureRules = this.changeSignatureRules.bind(this);
    this.changeRequireParameter = this.changeRequireParameter.bind(this);
  }
  
  componentDidMount() {
    window.addEventListener("message", (event) => {
      console.log( '我是react,我接受到了来自iframe的模型：', event.data );
      if(event !== undefined && event.data){
        if (event.data) {
          const newState = {};
          if (event.data.complete) {
            newState.complete = event.data.complete;
          }
          if (event.data.save) {
            // signatureAction.loadSignedPDF({sessionID: this.state.sessionID}, (result)=> {
              // var obj = window.open('');
              // obj.document.write(response);  
            // });
            this.setState({
              iframeSrc: `${config.esign_backend}/eSignature/loadSignedPDF?sessionID=${this.state.sessionID}`,
              iframeOpen: true,
              loading: true
            });
            // window.open(`${config.esign_backend}/eSignature/loadSignedPDF?sessionID=${this.state.sessionID}`);
          }
        }
      }
    }, false);
  }
  
  readerFile = (uploadFile, callback) => {
    var reader = new FileReader();
    reader.readAsDataURL(uploadFile);
    reader.onload = function () {
      callback(reader.result);
    };
    reader.onerror = function (error) {
      this.setState({
        binary: null,
        docID: null
      });
      alert('Error: ', error);
    };
  }

  handleUploadImage(event) {
    if (event.target.files === undefined || event.target.files.length !== 1) {
      alert("Please select an pdf");
    } else {
      if (event.target.files[0].type.indexOf("pdf") > -1) {
        const uploadFile = event.target.files[0];
        this.readerFile(uploadFile, (pdfBase64) => {
          const file = document.getElementById("userImage");
          file.value = '';
          this.setState({
            docID: uploadFile.name.replace(/(.*\/)*([^.]+).*/ig,"$2"),
            binary: pdfBase64,
            error: Object.assign({}, this.state.error, {binary: null})
          });
        });
      } else {
        const file = document.getElementById("userImage");
        file.value = '';
        this.setState({
          binary: null,
          docID: null
        });
        alert("File type is incorrect");
      }
    }
  }

  changeSignatureRules(editor, data, value) {
    if (editor.doc) {
      const lines = editor.doc.children[0].lines;
      let rules = null;
      _.forEach(lines, (line) => {
        if (!rules) {
          rules = line.text;
        } else {
          rules = `${rules}\n${line.text}`
        }
      });
      this.setState({ signatureRules: rules });
    }
  }

  changeRequireParameter(editor, data, value) {
    if (editor.doc) {
      const lines = editor.doc.children[0].lines;
      let rules = null;
      _.forEach(lines, (line) => {
        if (!rules) {
          rules = line.text;
        } else {
          rules = `${rules}\n${line.text}`
        }
      });
      this.setState({ requiredParameter: rules });
    }
  }

  checkValidation = () => {
    const {
      binary,
      signatureRules,
      requiredParameter
    } = this.state;
    const error = {};
    if (!binary) {
      error.binary = ["please upload pdf or use our template."];
    }
    if (!this.strToJSON(`[${signatureRules}]`)) {
      error.signatureRules = ["Error detected. please check."];
    }
    if (!this.strToJSON(`${requiredParameter}`)) {
      error.requiredParameter = ["Error detected. please check."];
    }
    this.setState({
      error
    });
    return _.isEmpty(error);
  }

  strToJSON = (jsonStr) => {
    try {
      var obj = JSON.parse(jsonStr);
      if (typeof obj == 'object' && obj) {
        return obj;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  process = (type) => {
    const { binary, signatureRules, requiredParameter } = this.state;
    if (this.checkValidation()) {
      const requiredParam = this.strToJSON(`${requiredParameter}`);
      const data = Object.assign({
        binary: binary.slice(binary.indexOf(',') + 1),
        signatureRules: this.strToJSON(`[${signatureRules}]`)
      }, requiredParam);

      this.setState({
        loading: true,
        iframeSrc: null
      });
      signatureAction.pross(data, (result) => {
        if (result.Status === "Success") {
          if (type === "iframe") {
            this.setState({
              sessionID: result.sessionID,
              iframeSrc: `${config.esign_frontend}`,
              iframeOpen: true,
            }, () => {
              const childFrameObj = document.getElementById('esignatureFrontend');
              childFrameObj.contentWindow.postMessage({sessionID: result.sessionID}, '*');
              this.setState({
                loading: false
              });
            });
          } else if (type === "newPage") {
            window.open(`${config.esign_frontend}/?sessionID=${result.sessionID}`, "Map");
          }
        } else {
          let error = {};
          if (result.Status === "Fail") {
            if (result.exception.length > 0) {
              _.forEach(result.exception, (item) => {
                if (item.reasonCD === "007" && item.subCode === "002") {
                  if (!error.binary) {
                    error.binary = [];
                  }
                  error.binary.push(`${item.reasonTXT}: ${item.subReasonTXT}`);
                } else if (item.reasonCD === "007") {
                  if (!error.requiredParameter) {
                    error.requiredParameter = [];
                  }
                  error.requiredParameter.push(`${item.reasonTXT}: ${item.subReasonTXT}`);
                } else if (item.reasonCD === "005") {
                  if (!error.signatureRules) {
                    error.signatureRules = [];
                  }
                  error.signatureRules.push(`${item.reasonTXT}: ${item.subReasonTXT}`);
                }
              });
            }
          }
          if (!error || _.isEmpty(error)) {
            if (!error.requiredParameter) {
              error.requiredParameter = [];
            }
            error.requiredParameter.push("Server Error!");
          }
          this.setState({
            error,
            loading: false
          });
        }
      });
    }
  }

  onDocumentLoadSuccess = ({ numPages }) => {//numPages是总页数
    this.setState({ numPages });
  };

  render() {
    const { classes } = this.props;
    const { iframe, iframeOpen, iframeSrc, error, binary, docID, sessionID, loading } = this.state;
    return (
      <Grid className={classes.root}>
        <Grid className={classes.container} container direction="row" justify="center" alignItems="center">
          <Grid className={classes.title} container direction="row" justify="center" alignItems="center">
            eSignature
          </Grid>
          <Grid className={classes.main}>
            <Grid className={classes.stepBox}>
              <Grid className={classes.steplabel}>
                Step 1 - upload PDF
              </Grid>
              <Grid className={classes.uploadPdf} container direction="row" justify="flex-start" alignItems="center">
                <img className={classes.pdfIcon} src={pdfIcon} alt={""} />
                <ButtonBase className={classes.uploadPdfButton} onClick={() => {
                  document.getElementById("userImage").click();
                }}>
                  Upload PDF
                </ButtonBase>
                <ButtonBase className={classes.uploadPdfButton} onClick={() => {
                  const file = document.getElementById("userImage");
                  file.value = '';
                  this.setState({
                    docID: "sample-template",
                    binary : util.getourTemplate(),
                    error: Object.assign({}, error, {binary: null})
                  });
                }}>
                  Use our template
                </ButtonBase>
              </Grid>
              {!docID ? null :
              <Grid className={classes.uploadMsg} container direction="row" alignItems="center"> 
                <Grid>
                  {`${docID}.pdf`}
                </Grid>
                <ButtonBase style={{
                  width: "20px", height: "20px", marginLeft: "10px", marginTop: "-3px"
                }} onClick={()=> {
                  this.setState({
                    binary: null,
                    docID: null
                  });
                }}>
                  <CancelPresentationIcon />
                </ButtonBase>
              </Grid>}
              <Grid className={classes.error}>
                {error && error.binary ? error.binary.map((item, inx) => {
                  return <Grid key={inx}>{item}</Grid>
                }) : <Grid>&nbsp;</Grid>}
              </Grid>
            </Grid>
            <Grid className={classes.stepBox}>
              <Grid className={classes.steplabel}>
                Step 2 - signature rules <Link className={classes.link}>Need help?</Link>
              </Grid>
              <MarkdownEditor
                style={{ color: "red" }}
                visble={false}
                height={200}
                toolbars={null}
                toolbarsMode={null}
                value={tempRules}
                onChange={this.changeSignatureRules}
              />
              <Grid className={classes.error}>
                {error && error.signatureRules ? error.signatureRules.map((item, inx) => {
                  return <Grid key={inx}>{item}</Grid>
                }) : <Grid>&nbsp;</Grid>}
              </Grid>
            </Grid>
            <Grid className={classes.stepBox}>
              <Grid className={classes.steplabel}>
                Step 3 - required parameter <Link className={classes.link}>Need help?</Link>
              </Grid>
              <MarkdownEditor
                style={{ color: "red" }}
                visble={false}
                height={100}
                toolbars={null}
                toolbarsMode={null}
                value={tempParam}
                onChange={this.changeRequireParameter}
              />
              <Grid className={classes.error}>
                {error && error.requiredParameter ? error.requiredParameter.map((item, inx) => {
                  return <Grid key={inx}>{item}</Grid>
                }) : <Grid>&nbsp;</Grid>}
              </Grid>
              <Grid style={{ width: "100%" }} container direction="row" justify="center" alignItems="center">
                {
                  loading ? <CircularProgress className={classes.loading}/> : 
                  <Grid>
                    <ButtonBase className={classes.process} onClick={() => {
                      this.process("iframe");
                    }}>
                      Process to iframe
                    </ButtonBase>
                    <ButtonBase className={classes.process} onClick={() => {
                      this.process("newPage");
                    }}>
                      Process to new page
                    </ButtonBase>
                  </Grid>
                }
              </Grid>
            </Grid>
            {!iframe || !iframeSrc || !iframeOpen ? null :
              <Grid className={classes.iframes}>
                  <iframe
                    id={"esignatureFrontend"}
                    className={classes.iframe}
                    title={"esignature"}
                    style={{border:"1px solid ",width:"100%",height:"calc(100% - 60px)", display: iframeSrc ===null ? "none" : "block" }}
                    src={iframeSrc}
                    onLoad={()=>{
                      const childFrameObj = document.getElementById('esignatureFrontend');
                      childFrameObj.contentWindow.postMessage({sessionID: sessionID}, '*'); //window.postMessage
                      this.setState({
                        loading: false
                      });
                    }}
                  />
              </Grid>
            }
            <Grid className={classes.end}></Grid>
          </Grid>
        </Grid>
        <input id={"userImage"} type={"file"} style={{ display: "none" }} onChange={this.handleUploadImage} accept={".pdf"} />
      </Grid>
    );
  }
}

Demo.propTypes = {};
export default connect(mapStateToProps)(withStyles(styles)(Demo));
