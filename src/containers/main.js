import React, { Component, cloneElement } from "react";
import PropTypes from "prop-types";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';
var signatureAction = require('../action/signatureAction');

class Main extends Component {

  render() {
    const { children } = this.props;
    return <section>{cloneElement(children, { key: "main" })}</section>;
  }
}

Main.propTypes = {
  children: PropTypes.any
};

export default connect(mapStateToProps)(Main);
