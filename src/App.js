import React, { Component } from "react";
import { Router } from "@reach/router";
import './reset.css';
// Routes
import Error from "./containers/error";
import Main from "./containers/main";
import Demo from "./containers/demo";

export default class App extends Component {

  render() {
    return (
        <Router>
          <Main path={`/`}>
            <Demo path={`/`} />
            <Error default />
          </Main>
        </Router>
    );
  }
}