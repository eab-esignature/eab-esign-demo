const getConfig = () => {
    const config = {
        esign_frontend: "http://10.0.0.147:5000",
        esign_backend: "http://10.0.0.164:8080"
    };
    return config;
}

export default getConfig();