import axios from 'axios';

export function process(data) {
    return new Promise((resolve) => {
        axios({
            method: 'post',
            url: 'eSignature/session_request',
            data: data,
            headers: {
                "Content-Type": "application/json"
            }
        }).then((response) => {
            resolve(response.data);
        }).catch((error) => {
            console.log("error: ", error);
            resolve(false);
        })
    });
}

export function loadSignedPDF(data) {
    return new Promise((resolve) => {
        axios({
            method: 'get',
            url: `eSignature/loadSignedPDF`,
            params: data,
            headers: {
                "Content-Type": "text/html"
            }
        }).then((response) => {
            var obj = window.open("about:blank");
            obj.document.write(response.data);
            resolve(response);
        }).catch((error) => {
            console.log("error: ", error);
            resolve(false);
        })
    });
}
