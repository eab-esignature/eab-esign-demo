var actionObj = require('../action/projectAction');

export const pross = (data, callback) => {
    actionObj.process(data).then((result) => {
        callback(result);
    });
}

export const loadSignedPDF = (data, callback) => {
    actionObj.loadSignedPDF(data).then((result) => {
        callback(result);
    });
}