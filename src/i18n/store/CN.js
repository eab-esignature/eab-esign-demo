const getTextStore = () => {
    const textStore = {};

    textStore.signPagination = {};
    textStore.signPagination.page = "页码";

    textStore.signView = {};
    textStore.signView.signatureBox = {};
    textStore.signView.signatureBox.clickTip = "Click here to sign";

    textStore.signCanvas = {};
    textStore.signCanvas.title = "请在此处签名";
    textStore.signCanvas.previousSignature = "使用上次完成的签名";
    textStore.signCanvas.clear = "清空";
    textStore.signCanvas.undo = "后退";
    textStore.signCanvas.cannel = "取消";
    textStore.signCanvas.apply = "应用";

    textStore.signSideBar = {};
    textStore.signSideBar.title = "签名列表";
    textStore.signSideBar.signBtn = "签名";
    textStore.signSideBar.saveSignature = "保存签名";

    textStore.msgDialog = {};
    textStore.msgDialog.saveTitle = "保存签名";
    textStore.msgDialog.saveMsg = "你希望保存吗?";
    textStore.msgDialog.cannel = "取消";
    textStore.msgDialog.save = "保存";
    return textStore;
}

export default getTextStore();
