import { CONTEXT } from "./REDUCER_TYPES";

export default {
  [CONTEXT]: {
    // GENERAL ACTION
    ADD: `${CONTEXT}/ADD`,
    RESET: `${CONTEXT}/RESET`,
    // VAR DEPENDENT ACTION
  }
};
