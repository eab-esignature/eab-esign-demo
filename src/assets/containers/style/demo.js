const styles = theme => ({
    root: {
        position: "fixed",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: "100%",
        height: "100%",
        backgroundColor: "rgba(237, 237, 237, 1)"
    },
    container: {
        height: "100%",
        width: "100%"
    },
    title: {
        position: "fixed",
        top: 0,
        width: "100%",
        height: "60px",
        color: "white",
        fontSize: "19px",
        backgroundColor: "rgba(178, 14, 56, 1)",
    },
    iframe: {
        // height: "auto",
    },
    main: {
        position: "fixed",
        top: "60px",
        paddingTop: "20px",
        height: "calc(100% - 80px)",
        width: "100%",
        overflow: "auto",
    },
    stepBox: {
        width: "900px",
        margin: "auto"
    },
    iframes: {
        marginTop: "60px",
        width: "900px",
        height: "900px",
        margin: "auto"
    },
    steplabel: {
        marginTop: "20px",
        marginBottom: "10px",
        fontSize: "19px"
    },
    uploadPdf: {
        padding: "20px",
        borderRadius: "6px",
        border: "1pt dashed rgba(112, 112, 112, 1)",
    },
    pdfIcon: {
        width: "60px",
        height: "60px",
        marginRight: "15px",
    },
    uploadPdfButton: {
        padding: "10px 20px",
        margin: "0px 10px",
        fontSize: "14px",
        fontWeight: "bold",
        borderRadius: "3px",
        border: "1px solid rgba(112, 112, 112, 1)"
    },
    rulesPannel: {
        height: "270px",
        color: "rgba(209, 209, 209, 1)",
        borderRadius: "6px",
        backgroundColor: "rgba(32, 32, 32, 1)",
    },
    link: {
        fontSize: "13px",
        margin: "0px 5px",
        color: "rgba(36, 126, 220, 1)"
    },
    error: {
        margin: "5px",
        fontSize: "15px",
        color: "rgba(178, 14, 56, 1)"
    },
    uploadMsg: {
        marginTop: "-27px",
        marginLeft: "110px",
        marginBottom: "15px",
        fontSize: "14px",
        color: "rgba(36, 126, 220, 1)"
    },
    loading: {
        // padding: "12px 35px",
        margin: "0px 70px",
        marginTop: "35px",
        marginBottom: "2px",
        color: "rgba(178, 14, 56, 1)",
    },
    process: {
        padding: "12px 35px",
        margin: "0px 70px",
        marginTop: "35px",
        color: "white",
        fontSize: "16px",
        borderRadius: "3px",
        backgroundColor: "rgba(178, 14, 56, 1)",
    },
    end: {
        paddingTop: "40px"
    }
});
export default styles;
  